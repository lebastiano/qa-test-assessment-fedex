Feature: Search for a Star Wars character

    Scenario: By full name
        Given the user opens the home page
        When the user searches for person "Luke Skywalker"
        Then the user sees the details of person "Luke Skywalker"

    Scenario: Not a valid character
        Given the user opens the home page
        When the user searches for person "Mr T"
        Then person cannot be found
