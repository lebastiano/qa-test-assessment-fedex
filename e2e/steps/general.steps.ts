const { Given } = require('cucumber');
const { browser } = require('protractor');

Given('the user opens the home page', async () => {
  await browser.get('/')
});
