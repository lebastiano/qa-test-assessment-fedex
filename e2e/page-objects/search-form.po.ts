import { $ } from 'protractor';

class SearchFormPO {
  private searchFormComponent = $('app-search-form');
  private characterComponent = $('app-character');
  private planetComponent = $('app-planet');

  private input = this.searchFormComponent.$('#query');
  private searchBtn = this.searchFormComponent.$('[data-test="search-button"]');
  private peopleRadioBtn = this.searchFormComponent.$('#people');
  private planetsRadioBtn = this.searchFormComponent.$('#planets');
  private characterNames = this.characterComponent.$$('[data-test="person-name"]');
  private planetNames = this.planetComponent.$$('[data-test="planet-name"]');

  get notFoundElement() { return $('[data-test="not-found"]') }

  async searchPeople(name: string): Promise<void> {
    await this.peopleRadioBtn.click();
    await this.performSearch(name);
  }

  async searchPlanets(name: string): Promise<void> {
    await this.planetsRadioBtn.click();
    await this.performSearch(name);
  }

  async getPeopleNames(): Promise<string[]> {
    return this.characterNames.map(item => item.getText());
  }

  async getPlanetNames(): Promise<string[]> {
    return this.planetNames.map(item => item.getText());
  }

  private async performSearch(query: string): Promise<void> {
    await this.input.sendKeys(query);
    await this.searchBtn.click();
  }
}

export default new SearchFormPO();
